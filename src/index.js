import './assets/css/style.scss';

import $ from 'jquery';
import './assets/js/modernizr.js';
import './assets/js/select2.min.js';

// @todo: add fancybox to images
// @todo: add lazy load

$(function () {
  $('.no-js').removeClass('no-js');

  $('.js-select').select2({
    minimumResultsForSearch: -1
  });


  // scroll
  var $goTop = $('.js-go-top');
  function onScroll() {
    $goTop.toggleClass('visible', $(document).scrollTop() > 565);
  }
  onScroll();
  $(document).on('scroll', onScroll);

  $goTop.on('click', function () {
    $('html, body').stop(true, true).animate({
      scrollTop: 0
    }, 300);
  });


  // collapses
  $('.js-collapse-block').each(function () {
    var $collapseBlock = $(this);
    var $collapseLink = $collapseBlock.find('.js-collapse-link');

    function expandFilter() {
      $collapseBlock.addClass('visible');
      $collapseLink.each(function () {
        $(this).html($(this).data('text-expand'));
      });
    }

    function collapseFilter() {
      $collapseBlock.removeClass('visible');
      $collapseLink.each(function () {
        $(this).html($(this).data('text-collapse'));
      });
    }

    if ($collapseBlock.data('expand-on-click')) {
      $collapseBlock.on('click', function () {
        expandFilter();
      });
    }

    $collapseLink.on('click', function (evt) {
      evt.stopPropagation();

      if ($collapseBlock.hasClass('visible')) {
        collapseFilter();
      } else {
        expandFilter();
      }
    });
  });
});
