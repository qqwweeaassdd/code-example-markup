const path = require('path');

const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: ['./src/index.js'],
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist'
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          }, {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          }, {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      }, {
        test: /\.(png|svg|jpg|gif|ico)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]?[hash]',
              context: 'src'
            }
          }
        ]
      }, {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]?[hash]',
              context: 'src'
            }
          }
        ]
      }, {
         test: /\.(csv|tsv)$/,
         use: [
           'csv-loader'
         ]
      }, {
         test: /\.xml$/,
         use: [
           'xml-loader'
         ]
      }, {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              interpolate: true
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css?[hash]",
      chunkFilename: "[id].css"
    })
  ]
};

['index', 'font-detail', 'about', 'contacts', 'install-fonts'].forEach(item => {
  module.exports.plugins.push(new HtmlWebPackPlugin({
    template: `./src/${item}.html`,
    filename: `./${item}.html`,
    minify: {
      removeScriptTypeAttributes: true,
    }
  }));
});
